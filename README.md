# 基于Spring Boot的高校社团管理系统

## 项目简介

本项目是一个基于Spring Boot开发的高校社团管理系统。系统采用RESTful风格设计，实现了多级权限控制，并融入了短信接口功能。主要功能模块包括会员管理、活动管理、场地管理、会议管理、社团管理、消息通知和文件下发等。

## 功能模块

### 1. 会员管理
- 会员信息录入与维护
- 会员权限管理
- 会员活动参与记录

### 2. 活动管理
- 活动发布与编辑
- 活动报名与签到
- 活动评价与反馈

### 3. 场地管理
- 场地预约与管理
- 场地使用记录
- 场地维护与检查

### 4. 会议管理
- 会议安排与通知
- 会议记录与归档
- 会议参与人员管理

### 5. 社团管理
- 社团创建与管理
- 社团成员管理
- 社团活动与会议管理

### 6. 消息通知
- 系统消息推送
- 短信通知接口
- 消息记录与查看

### 7. 文件下发
- 文件上传与下载
- 文件权限管理
- 文件版本控制

## 技术栈

- **后端框架**: Spring Boot
- **数据库**: MySQL
- **权限控制**: Spring Security
- **RESTful API**: Spring MVC
- **短信接口**: 第三方短信服务
- **前端**: HTML, CSS, JavaScript

## 安装与运行

### 环境要求
- JDK 1.8 或更高版本
- Maven 3.x
- MySQL 5.7 或更高版本

### 安装步骤
1. 克隆项目到本地
   ```bash
   git clone https://github.com/your-repo/springboot-club-management.git
   ```
2. 导入项目到IDE（如IntelliJ IDEA）
3. 配置数据库连接信息，修改`application.properties`文件中的数据库配置
   ```properties
   spring.datasource.url=jdbc:mysql://localhost:3306/club_management
   spring.datasource.username=your-username
   spring.datasource.password=your-password
   ```
4. 运行项目
   ```bash
   mvn spring-boot:run
   ```
5. 访问系统
   ```
   http://localhost:8080
   ```

## 贡献

欢迎大家提交Issue和Pull Request，共同完善本项目。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

## 联系我们

如有任何问题或建议，请联系项目维护者：
- 邮箱：your-email@example.com
- GitHub：[your-github-profile](https://github.com/your-github-profile)